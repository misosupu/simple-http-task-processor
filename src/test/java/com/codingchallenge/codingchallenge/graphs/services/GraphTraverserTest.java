package com.codingchallenge.codingchallenge.graphs.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.codingchallenge.codingchallenge.graphs.exceptions.GraphNotAcyclicException;
import com.codingchallenge.codingchallenge.graphs.models.DirectedGraph;
import com.codingchallenge.codingchallenge.graphs.models.Vertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

class GraphTraverserTest {

  GraphTraverser graphTraverser;

  @BeforeEach
  void beforeEach() {
    graphTraverser = new GraphTraverser();
  }

  @Test
  public void givenCyclicGraph_whenPerformingTopologicalSort_shouldThrowException() {
    DirectedGraph<String> graph = new DirectedGraph<>();

    Vertex<String> l = new Vertex<>("1", "L");
    Vertex<String> o = new Vertex<>("2", "O");
    Vertex<String> p = new Vertex<>("3", "P");

    graph.addLeaf(l);
    graph.addLeaf(o);
    graph.addLeaf(p);

    graph.addEdge(l, o);
    graph.addEdge(o, o);
    graph.addEdge(o, p);

    assertThrows(GraphNotAcyclicException.class, () ->
      graphTraverser.topologicalSort(graph)
    );
  }

  @Test
  public void givenAcyclicGraph_whenPerformingTopologicalSort_shouldOrderVerticalCorrectly() {
    DirectedGraph<Integer> graph = new DirectedGraph<>();

    Vertex<Integer> first = new Vertex<>("1", 4);
    Vertex<Integer> second = new Vertex<>("2", 8);
    Vertex<Integer> third = new Vertex<>("3", 15);

    graph.addLeaf(first);
    graph.addLeaf(second);
    graph.addLeaf(third);

    graph.addEdge(first, second);
    graph.addEdge(second, third);

    List<Vertex<Integer>> topologicalSort = graphTraverser.topologicalSort(graph);

    assertEquals(
        List.of(15, 8, 4),
        topologicalSort.stream().map(Vertex::getData).collect(Collectors.toList())
    );
  }

  @Test
  public void givenCyclicGraph_whenPerformingDepthFirstSearch_shouldDetectCycles() {
    DirectedGraph<Boolean> graph = new DirectedGraph<>();

    Vertex<Boolean> first = new Vertex<>("1", true);
    Vertex<Boolean> second = new Vertex<>("2", false);

    graph.addLeaf(first);
    graph.addLeaf(second);

    graph.addEdge(first, second);
    graph.addEdge(second, first);

    graphTraverser.DFS(graph);

    assertTrue(graph.isContainingCycle());
  }
}
