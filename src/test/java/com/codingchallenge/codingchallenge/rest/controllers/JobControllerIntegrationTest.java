package com.codingchallenge.codingchallenge.rest.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.file.Files;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class JobControllerIntegrationTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  ObjectMapper objectMapper;

  @Test
  public void givenDeterminizedTasks_whenPrioritized_returnsInExpectedOrder() throws Exception {

    ClassPathResource providedTasksResource = new ClassPathResource("fixtures/determinized_tasks_provided.json");
    byte[] tasksResourceContent = Files.readAllBytes(providedTasksResource.getFile().toPath());

    ClassPathResource expectedTasksResource = new ClassPathResource("fixtures/determinized_tasks_expected.json");
    String expectedResult = new String(Files.readAllBytes(expectedTasksResource.getFile().toPath()));

    mockMvc.perform(post("/v1/jobs/prioritize")
        .contentType(MediaType.APPLICATION_JSON)
        .content(tasksResourceContent))
        .andExpect(MockMvcResultMatchers.content().json(expectedResult));
  }
}
