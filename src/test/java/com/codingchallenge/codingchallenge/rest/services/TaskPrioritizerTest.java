package com.codingchallenge.codingchallenge.rest.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.codingchallenge.codingchallenge.graphs.models.DirectedGraph;
import com.codingchallenge.codingchallenge.graphs.models.Vertex;
import com.codingchallenge.codingchallenge.graphs.services.GraphTraverser;
import com.codingchallenge.codingchallenge.rest.models.Task;
import com.codingchallenge.codingchallenge.rest.services.tasks.TaskPrioritizer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
class TaskPrioritizerTest {

  @Mock
  GraphTraverser graphTraverser;

  private TaskPrioritizer taskPrioritizer;

  final ArgumentCaptor<DirectedGraph<Task>> captor = ArgumentCaptor.forClass(DirectedGraph.class);

  @BeforeEach
  void beforeEach() {
    MockitoAnnotations.openMocks(this);
    taskPrioritizer = new TaskPrioritizer(graphTraverser);
  }

  @Test
  public void givenTaskRefersToNonExistentTask_whenBuildingGraph_shouldThrowException() {
    Task parse = new Task();
    parse.setName("SQL Query Parser");
    parse.setCommand("parse");

    Task optimize = new Task();
    optimize.setName("SQL Query Optimizer");
    optimize.setCommand("optimize");
    optimize.setDependencies(List.of("parse", "compile"));

    List<Task> tasks = List.of(optimize, parse);

    assertThrows(ResponseStatusException.class, () ->
        taskPrioritizer.prioritize(tasks)
    );
  }

  @Test
  public void givenNonCyclicDependentSetOfTasks_shouldArrangeThemAccordingly() {
    Task lexicalAnalyzerTask = new Task();
    lexicalAnalyzerTask.setName("Lexical Analyzer");
    lexicalAnalyzerTask.setCommand("Some command");

    Task syntaxAnalyzerTask = new Task();
    syntaxAnalyzerTask.setName("Syntax Analyzer");
    syntaxAnalyzerTask.setCommand("Some other command");
    syntaxAnalyzerTask.setDependencies(Collections.singletonList(lexicalAnalyzerTask.getName()));

    Task codeGeneratorTask = new Task();
    codeGeneratorTask.setName("Code Generator");
    codeGeneratorTask.setCommand("Another command");
    codeGeneratorTask.setDependencies(
        List.of(lexicalAnalyzerTask.getName(), syntaxAnalyzerTask.getName()));

    List<Task> tasks = List.of(lexicalAnalyzerTask, codeGeneratorTask, syntaxAnalyzerTask);

    DirectedGraph<Task> expectedGraph = new DirectedGraph<>();

    Vertex<Task> lexicalAnalyzer = new Vertex<>(lexicalAnalyzerTask.getName(), lexicalAnalyzerTask);
    Vertex<Task> syntaxAnalyzer = new Vertex<>(syntaxAnalyzerTask.getName(), syntaxAnalyzerTask);
    Vertex<Task> codeGenerator = new Vertex<>(codeGeneratorTask.getName(), codeGeneratorTask);

    expectedGraph.addEdge(syntaxAnalyzer, lexicalAnalyzer);
    expectedGraph.addEdge(codeGenerator, syntaxAnalyzer);
    expectedGraph.addEdge(codeGenerator, lexicalAnalyzer);

    List<Vertex<Task>> taskVertices = List.of(lexicalAnalyzer, syntaxAnalyzer, codeGenerator);
    when(graphTraverser.topologicalSort(any(DirectedGraph.class)))
        .thenReturn(taskVertices);

    List<Task> prioritized = taskPrioritizer.prioritize(tasks);

    assertEquals(
        List.of("Lexical Analyzer", "Syntax Analyzer", "Code Generator"),
        prioritized.stream().map(Task::getName).collect(Collectors.toList())
    );

    verify(graphTraverser).topologicalSort(captor.capture());
    assertEquals(taskVertices, new ArrayList<>(captor.getValue().getVertices()));
  }

  @Test
  public void givenNonCyclicDependentSetOfTasks_whenRequestingShellOutput_shouldReturnCorrectScript() {
    Task createDirectoryTask = new Task();
    createDirectoryTask.setName("Create Directory");
    createDirectoryTask.setCommand("mkdir test");

    Task changeDirectoryTask = new Task();
    changeDirectoryTask.setName("Change Directory");
    changeDirectoryTask.setCommand("cd test");
    changeDirectoryTask.setDependencies(Collections.singletonList(createDirectoryTask.getName()));

    List<Task> tasks = List.of(changeDirectoryTask, createDirectoryTask);

    DirectedGraph<Task> expectedGraph = new DirectedGraph<>();

    Vertex<Task> createDirectory = new Vertex<>(createDirectoryTask.getName(), createDirectoryTask);
    Vertex<Task> changeDirectory = new Vertex<>(changeDirectoryTask.getName(), changeDirectoryTask);

    expectedGraph.addEdge(changeDirectory, createDirectory);

    List<Vertex<Task>> taskVertices = List.of(createDirectory, changeDirectory);
    when(graphTraverser.topologicalSort(any(DirectedGraph.class)))
        .thenReturn(taskVertices);

    String prioritized = taskPrioritizer.getPrioritizedTasksScript(tasks);

    assertEquals(
        "#!/usr/bin/env bash\n" +
            "mkdir test\n" +
            "cd test",
        prioritized
    );

    verify(graphTraverser).topologicalSort(captor.capture());
    assertThat(taskVertices, containsInAnyOrder(captor.getValue().getVertices().toArray()));
  }
}
