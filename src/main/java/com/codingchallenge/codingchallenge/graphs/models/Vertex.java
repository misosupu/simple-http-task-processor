package com.codingchallenge.codingchallenge.graphs.models;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
public class Vertex<T> {

  String id;

  private final T data;

  // needed for traversal algorithms to work
  private DiscoveryState state;

  @Setter
  private int discoveryTime;
  @Setter
  private int fullyDiscoveredTime;

  public Vertex(String id, T data) {
    this.id = id;
    this.state = DiscoveryState.UNDISCOVERED;
    this.data = data;
  }

  public void markAsUndiscovered() {
    this.state = DiscoveryState.UNDISCOVERED;
  }

  public void markAsVisited() {
    this.state = DiscoveryState.VISITED;
  }

  public void markAsDiscovered() {
    this.state = DiscoveryState.DISCOVERED;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Vertex)) {
      return false;
    }

    Vertex<?> vertex = (Vertex<?>) obj;
    return Objects.equals(id, vertex.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
