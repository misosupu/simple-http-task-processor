package com.codingchallenge.codingchallenge.graphs.models;

/**
 * Denotes vertex state during graph traversal
 */
public enum DiscoveryState {
  UNDISCOVERED,
  DISCOVERED,
  VISITED
}
