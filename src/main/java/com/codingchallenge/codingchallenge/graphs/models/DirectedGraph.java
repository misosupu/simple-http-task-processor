package com.codingchallenge.codingchallenge.graphs.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implements a directed graph using an adjacency list representation.
 */
@Getter
@Setter
@NoArgsConstructor
public class DirectedGraph<T> implements Graph<T> {

  private Map<Vertex<T>, List<Vertex<T>>> adjacencyList = new HashMap<>();

  private boolean containingCycle;

  public Set<Vertex<T>> getVertices() {
    return this.adjacencyList.keySet();
  }

  public List<Vertex<T>> getNeighbours(Vertex<T> source) {
    return this.adjacencyList.getOrDefault(source, Collections.emptyList());
  }

  public void addEdge(Vertex<T> source, Vertex<T> destination) {
    this.adjacencyList.computeIfAbsent(source, key -> new ArrayList<>()).add(destination);
  }

  public void addLeaf(Vertex<T> leaf) {
    this.adjacencyList.put(leaf, new ArrayList<>());
  }
}
