package com.codingchallenge.codingchallenge.graphs.models;

import java.util.List;
import java.util.Set;

public interface Graph<T> {

  Set<Vertex<T>> getVertices();
  void addEdge(Vertex<T> source, Vertex<T> destination);
  void addLeaf(Vertex<T> leaf);
  List<Vertex<T>> getNeighbours(Vertex<T> source);
}
