package com.codingchallenge.codingchallenge.graphs.exceptions;

public class GraphNotAcyclicException extends RuntimeException {

  public GraphNotAcyclicException() {
    super("Graph is not acyclic.");
  }
}
