package com.codingchallenge.codingchallenge.graphs.services;

import com.codingchallenge.codingchallenge.graphs.exceptions.GraphNotAcyclicException;
import com.codingchallenge.codingchallenge.graphs.models.DirectedGraph;
import com.codingchallenge.codingchallenge.graphs.models.DiscoveryState;
import com.codingchallenge.codingchallenge.graphs.models.Vertex;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class GraphTraverser {

  public <T> List<Vertex<T>> topologicalSort(DirectedGraph<T> graph) {
    this.DFS(graph);

    if (graph.isContainingCycle()) {
      // cannot do topological sort
      throw new GraphNotAcyclicException();
    }

    return new ArrayList<>(graph.getVertices())
        .stream()
        .sorted(Comparator.comparingInt(Vertex::getFullyDiscoveredTime))
        .collect(Collectors.toList());
  }

  // Depth-First Search algorithm implementation
  public <T> void DFS(DirectedGraph<T> graph) {
    Set<Vertex<T>> vertices = graph.getVertices();

    AtomicInteger time = new AtomicInteger(0);

    vertices.forEach(Vertex::markAsUndiscovered);

    vertices.forEach(vertex -> {
      if (vertex.getState() == DiscoveryState.UNDISCOVERED) {
        discover(vertex, graph, time);
      }
    });
  }

  // discover a single vertex and all others, which are reachable from it
  private <T> void discover(Vertex<T> source, DirectedGraph<T> graph, AtomicInteger time) {
    source.markAsDiscovered();
    source.setDiscoveryTime(time.getAndIncrement());

    List<Vertex<T>> neighbours = graph.getNeighbours(source);

    for (Vertex<T> neighbour : neighbours) {
      if (neighbour.getState() == DiscoveryState.UNDISCOVERED) {
        discover(neighbour, graph, time);
      } else if (neighbour.getState() == DiscoveryState.DISCOVERED) {
        // has back edge
        graph.setContainingCycle(true);
      }
    }

    source.markAsVisited();
    source.setFullyDiscoveredTime(time.getAndIncrement());
  }
}
