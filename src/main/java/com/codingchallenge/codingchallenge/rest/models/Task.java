package com.codingchallenge.codingchallenge.rest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {

  @NotNull
  private String name;

  @NotNull
  private String command;

  @NotNull
  @JsonProperty(value = "requires", access = JsonProperty.Access.WRITE_ONLY)
  private List<String> dependencies = List.of();
}
