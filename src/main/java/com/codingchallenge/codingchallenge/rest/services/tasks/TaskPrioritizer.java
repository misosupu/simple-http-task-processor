package com.codingchallenge.codingchallenge.rest.services.tasks;

import com.codingchallenge.codingchallenge.graphs.exceptions.GraphNotAcyclicException;
import com.codingchallenge.codingchallenge.graphs.models.DirectedGraph;
import com.codingchallenge.codingchallenge.graphs.models.Vertex;
import com.codingchallenge.codingchallenge.graphs.services.GraphTraverser;
import com.codingchallenge.codingchallenge.rest.models.Task;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class TaskPrioritizer {

  private final GraphTraverser graphTraverser;

  /**
   * This method internally builds a directed graph and then applies topological sort on it.
   *
   * @return Topologically sorted list of tasks
   */
  public List<Task> prioritize(List<Task> tasks) {
    List<Task> prioritized;

    try {
      DirectedGraph<Task> graph = buildGraph(tasks);
      List<Vertex<Task>> topologicalSort = graphTraverser.topologicalSort(graph);

      prioritized = topologicalSort
          .stream()
          .map(Vertex::getData)
          .collect(Collectors.toList());
    } catch (GraphNotAcyclicException | TaskNotFoundException e) {
      log.error(e.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
    }

    return prioritized;
  }

  public String getPrioritizedTasksScript(List<Task> tasks) {
    List<Task> prioritized = this.prioritize(tasks);

    final String shebangDirective = "#!/usr/bin/env bash";

    String script = prioritized.stream()
        .map(Task::getCommand)
        .collect(Collectors.joining(System.lineSeparator()));

    return shebangDirective
        .concat(System.lineSeparator())
        .concat(script);
  }

  /**
   * Constructs a graph, where each vertex holds a reference to the respective payload.
   */
  private DirectedGraph<Task> buildGraph(List<Task> tasks) {
    DirectedGraph<Task> graph = new DirectedGraph<>();
    // Mapping vertex id to actual payload
    Map<String, Vertex<Task>> idToVertexMap = new HashMap<>();
    tasks.stream()
        .filter(Objects::nonNull)
        .forEach(task -> idToVertexMap.put(task.getName(), new Vertex<>(task.getName(), task)));

    for (Map.Entry<String, Vertex<Task>> entry : idToVertexMap.entrySet()) {
      Vertex<Task> source = entry.getValue();
      graph.addLeaf(source);

      source.getData().getDependencies().forEach(
          destinationId -> {
            Vertex<Task> destination = idToVertexMap.computeIfAbsent(destinationId, vertex -> {
              throw new TaskNotFoundException(source.getId(), destinationId);
            });

            graph.addEdge(source, destination);
          }
      );
    }

    return graph;
  }
}
