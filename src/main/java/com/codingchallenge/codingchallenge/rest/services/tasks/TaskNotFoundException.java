package com.codingchallenge.codingchallenge.rest.services.tasks;

public class TaskNotFoundException extends RuntimeException {

  public TaskNotFoundException(String source, String destination) {
    super(String.format("Task with name %s not found (required by %s).", destination, source));
  }
}
