package com.codingchallenge.codingchallenge.rest.controllers;

import com.codingchallenge.codingchallenge.rest.models.Task;
import com.codingchallenge.codingchallenge.rest.services.tasks.TaskPrioritizer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/v1/jobs")
@RequiredArgsConstructor
public class JobController {

  private final TaskPrioritizer taskPrioritizer;

  @PostMapping(
      value = "/prioritize"
  )
  public List<Task> prioritize(@Valid @RequestBody List<Task> tasks) {
    return this.taskPrioritizer.prioritize(tasks);
  }

  @PostMapping(
      value = "/prioritize/shell",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.TEXT_PLAIN_VALUE
  )
  public String getPrioritizedScript(@Valid @RequestBody List<Task> tasks) {
    return this.taskPrioritizer.getPrioritizedTasksScript(tasks);
  }
}
