# Simple HTTP job processing service

Implementation of a rather contrived HTTP job processing service.

A job is a collection of tasks, where each task has a name and a shell command. Tasks may
depend on other tasks and require that those are executed beforehand. The service takes care
of sorting the tasks to create a proper execution order.

# Getting Started

The v1 API supports two endpoints for arranging а task execution order, according to their respective dependencies:

# Application Startup

## Dependencies

The application requires the following prerequisites for building and running:
* Gradle 6 or higher
* JDK 11


## Building and running locally

The project can be run directly using the standard Gradle run task:

```shell
./gradlew bootRun
```

Or built and executed via:

```shell
./gradlew clean build
java -jar build/libs/coding-challenge-0.0.1-SNAPSHOT.jar
```

### Returning a sorted list based on the resolved execution order

```shell
curl -H "Content-Type: application/json" -d @tasks.json http://localhost:8080/api/v1/jobs/prioritize/
```

An example request payload for the above could be:

```json
[
  {
    "name": "task-1",
    "command": "touch /tmp/file1"
  },
  {
    "name": "task-2",
    "command": "cat /tmp/file1"
  },
  {
    "name": "task-3",
    "command": "echo 'Hello World!' > /tmp/file1",
    "requires": [
      "task-1"
    ]
  },
  {
    "name": "task-4",
    "command": "rm /tmp/file1",
    "requires": [
      "task-2",
      "task-3"
    ]
  }
]
```

### Returning a shell script with the task commands ordered according to their dependency requirements

```shell
curl -H "Content-Type: application/json" -d @tasks.json http://localhost:8080/api/v1/jobs/prioritize/shell | bash
```
